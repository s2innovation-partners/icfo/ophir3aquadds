FROM centos:7
COPY maxiv.repo /etc/yum.repos.d/
RUN yum install -y epel-release && yum makecache
RUN yum install -y \
    python-pytango \
    python-fandango \
    python-pip \
    && yum clean all
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY Ophir3AQUAD/Ophir3AQUAD.py /
ENTRYPOINT ["python", "/Ophir3AQUAD.py"]
