import setuptools

setuptools.setup(
    name="tangods-ophir3AQUAD",
    version="1.0.0",
    description="Device server for Ophir 3AQUAD",
    author="S2innovation",
    author_email="contact@s2innovation.com",
    license="Proprietary",
    classifiers=[
        'License :: Other/Proprietary License'],
    packages=setuptools.find_packages(),
    install_requires=['statistics'],
    entry_points={
        'console_scripts': [
            "Ophir3AQUAD=Ophir3AQUAD.Ophir3AQUAD:main",
        ]
    }

)

