import logging
import sys
from random import uniform
from statistics import mean

from tango.server import Device, attribute, device_property, DeviceMeta, command, run
from tango import DebugIt, DispLevel, AttrWriteType, DevState
from Ophir3AQUADlib import Ophir3AQUADController, Ophir3AQUADException

debug_it = DebugIt(show_args=True, show_kwargs=True, show_ret=True)


class Ophir3AQUAD(Device):  # pylint: disable=too-many-instance-attributes
    __metaclass__ = DeviceMeta

    # ---------------------------------
    #   Properties
    # ---------------------------------

    ReadPeriod = device_property(
        dtype=int,
        default_value=1000,
        doc="Polling period for reading data in ms for the Ophir BeamTrack sensor"
    )

    ReadPoints = device_property(
        dtype=int,
        default_value=100,
        doc="Number of collected entries for the Ophir BeamTrack sensor"
    )

    NormalizedPoints = device_property(
        dtype=int,
        default_value=1000,
        doc="Number of collected entries of power value for the Ophir BeamTrack sensor"
    )

    MeasurementUnit = device_property(
        dtype=str,
        default_value="mm",
        doc="""Unit of measurements of X, Y positions and Size of the BeamTrack sensor"""
    )

    Simulated = device_property(
        dtype=bool,
        default_value=False,
        doc="""Data generation mode. If True data are randomly generated.
         If False data are obtained from Ophir 3A-QUAD sensor"""
    )
    IP = device_property(
        dtype="str",
        mandatory=True,
        doc="IP of the device"
    )

    # ---------------------------------
    #   Global methods
    # ---------------------------------

    @debug_it
    def init_device(self):
        Device.init_device(self)
        self.set_state(DevState.INIT)
        self._x_raw = 0
        self._y_raw = 0
        self._size = 0
        self._reference_x = 0
        self._reference_y = 0
        self._power = 0
        self._power_history = [0] * (self.NormalizedPoints + 1)
        self._multiplier = 1

        self._x_hist = [0] * self.ReadPoints
        self._y_hist = [0] * self.ReadPoints
        self.set_state(DevState.ON)

        if not self.Simulated:
            self.sensor = Ophir3AQUADController(self.IP)
            try:
                self.sensor.connect()
            except Ophir3AQUADException:
                self.set_state(DevState.FAULT)
                self.error_stream(str(sys.exc_info()[1]))
                logging.error(str(sys.exc_info()[1]))
                raise sys.exc_info()[1]

    @debug_it
    def delete_device(self):
        self.sensor.disconnect()

    # ---------------------------------
    #   Attributes
    # ---------------------------------
    coord_x = attribute(label="X position",
                        dtype=float,
                        polling_period=ReadPeriod.default_value,
                        display_level=DispLevel.OPERATOR,
                        access=AttrWriteType.READ,
                        format="%8.2f",
                        fget="get_x_coord",
                        doc="The X position of the Ophir BeamTrack sensor")

    coord_y = attribute(label="Y position",
                        dtype=float,
                        polling_period=ReadPeriod.default_value,
                        display_level=DispLevel.OPERATOR,
                        access=AttrWriteType.READ,
                        format="%8.2f",
                        fget="get_y_coord",
                        doc="The Y position of the Ophir BeamTrack sensor")

    size = attribute(label="Size",
                     dtype=float,
                     polling_period=ReadPeriod.default_value,
                     display_level=DispLevel.OPERATOR,
                     access=AttrWriteType.READ,
                     format="%8.2f",
                     unit="mm",
                     fget="get_size",
                     doc="Size of the Ophir BeamTrack sensor in mm")

    power = attribute(label="Power",
                      dtype=float,
                      polling_period=ReadPeriod.default_value,
                      display_level=DispLevel.OPERATOR,
                      access=AttrWriteType.READ,
                      unit="W",
                      format="%5.4f",
                      fget="get_power",
                      doc="The power value in [W] for the Ophir BeamTrack sensor")

    multiplier = attribute(label="Multiplier",
                           dtype=float,
                           polling_period=ReadPeriod.default_value,
                           display_level=DispLevel.OPERATOR,
                           access=AttrWriteType.READ_WRITE,
                           format="%5.4f",
                           fget="get_multiplier",
                           fset='set_multiplier',
                           doc="The multiplier for calculating power value")

    power_normal = attribute(label="Power normalized",
                             dtype=float,
                             polling_period=ReadPeriod.default_value,
                             display_level=DispLevel.OPERATOR,
                             access=AttrWriteType.READ,
                             format="%5.4f",
                             fget="get_power_normalized",
                             doc="The normalized power value for the Ophir BeamTrack sensor")

    history_x = attribute(label="X position history",
                          dtype=(float,),
                          polling_period=ReadPeriod.default_value,
                          display_level=DispLevel.OPERATOR,
                          access=AttrWriteType.READ,
                          max_dim_x=ReadPoints.default_value,
                          fget="get_x_hist",
                          doc="History of X position from the Ophir BeamTrack sensor")

    history_y = attribute(label="Y position history",
                          dtype=(float,),
                          polling_period=ReadPeriod.default_value,
                          display_level=DispLevel.OPERATOR,
                          access=AttrWriteType.READ,
                          max_dim_x=ReadPoints.default_value,
                          fget="get_y_hist",
                          doc="History of Y position from the Ophir BeamTrack sensor")

    @command(
        polling_period=ReadPeriod.default_value
    )
    @debug_it
    def update_position_size(self):
        """
        update position of beam
        """
        if self.Simulated:
            self._x_raw = uniform(-10, 10)
            self._y_raw = uniform(-10, 10)
            self._size = uniform(0, 10)
        else:
            if self.MeasurementUnit == "mm":
                try:
                    self._x_raw, self._y_raw, self._size = self.sensor.read_x_y_size_mm()
                except Ophir3AQUADException:
                    self.warn_stream(str(sys.exc_info()[1]))

            else:
                try:
                    self._x_raw, self._y_raw, self._size = self.sensor.read_x_y_size_microns()
                except Ophir3AQUADException:
                    self.warn_stream(str(sys.exc_info()[1]))

    @debug_it
    def get_x_coord(self):
        if len(self._x_hist) >= self.ReadPoints:
            del self._x_hist[0]
        self._x_hist.append(self._x_raw)

        return self._x_raw - self._reference_x

    @debug_it
    def get_y_coord(self):
        if len(self._y_hist) >= self.ReadPoints:
            del self._y_hist[0]
        self._y_hist.append(self._y_raw)

        return self._y_raw - self._reference_y

    @debug_it
    def get_size(self):
        return self._size

    @command
    @debug_it
    def save_reference(self):
        """
        save x and y position as reference
        """
        self._reference_x = self._x_raw
        self._reference_y = self._y_raw

    @command(
        polling_period=ReadPeriod.default_value
    )
    @debug_it
    def fetch_power(self):
        """
        get power value
        """

        if self.Simulated:
            self._power = uniform(-10, 10)
        else:
            try:
                self._power = self.sensor.read_power()
            except Ophir3AQUADException:
                self.warn_stream(str(sys.exc_info()[1]))
                raise sys.exc_info()[1]

        del self._power_history[0]
        self._power_history.append(self._power)

    def calculate_normalized(self):
        avr_list = mean(self._power_history[:-1])
        if avr_list == 0:
            return 0
        return self._power / avr_list

    @debug_it
    def get_power_normalized(self):
        return self.calculate_normalized()

    @debug_it
    def set_multiplier(self, multiply):
        self._multiplier = multiply

    @debug_it
    def get_multiplier(self):
        return float(self._multiplier)

    @debug_it
    def get_power(self):
        return self._power * self._multiplier

    @debug_it
    def get_x_hist(self):
        return [point - self._reference_x for point in self._x_hist]

    @debug_it
    def get_y_hist(self):
        return [point - self._reference_y for point in self._y_hist]


def main(args=None, **kwargs):
    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.DEBUG)
    return run((Ophir3AQUAD,), args=args, **kwargs)


if __name__ == '__main__':
    main()
