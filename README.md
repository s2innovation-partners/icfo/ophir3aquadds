# Ophir 3A-QUAD 
[LINK](https://www.ophiropt.com/laser--measurement/laser-power-energy-meters/products/BeamTrack-Power-Position-Size-Sensors/BeamTrack-Power-Position-Size-Thermal-Sensors/3A-QUAD)

The BeamTrack series 3A-QUAD laser measurement sensor measures laser beam position well as power and energy. It has a 9.5mm aperture and can measure beam position to 0.15mm accuracy. It measures power from 100µW to 3W and energy from 20µJ to 2J.
